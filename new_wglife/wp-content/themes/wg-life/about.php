<?php
/*
Template Name: About us
*/

get_header(); ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
                    <?php $stat_bg = get_field('stat_bg','options'); 
                    if( !empty($stat_bg) ): ?>
                    <div class="full-container" style="background-image: url('<?php echo $stat_bg['url']; ?>')">
                        <div class="overlay"></div>
                        <div class="container">
                            <h1 class="main-title"><?php the_title(); ?></h1>
                        </div>
                    </div>
                    <?php endif; ?>
                    <div class="container breadcrumb">
                        <?php  if(function_exists('bcn_display')) {
                            bcn_display();
                        }?>
                    </div>
                    <?php while ( have_posts() ) : the_post(); ?>
                        <div class="container about-us">
                            <?php the_content(); ?>
                        </div>
                    <?php endwhile; ?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
