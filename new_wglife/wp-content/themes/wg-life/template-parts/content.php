<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package wg-life
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		</main><!-- #main -->
	</div><!-- #primary -->