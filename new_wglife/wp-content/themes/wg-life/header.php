<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package wg-life
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<header id="masthead" class="site-header">
            <div class="logo-slog">
		<div class="site-branding">
			<?php
			the_custom_logo(); ?>
		</div><!-- .site-branding -->
                <div class="logo-text">
                    <p class="bold-text">Загальнофанатський ресурс ФК Ворскла</p>
                    <p>Події, новини з життя клубу та фанатів</p>
                </div>
            </div>
            <div class="menu-bar">
                    <div class="social-lang">
                        <?php if( have_rows('social_icons','options') ): ?>
                        <div class="social-icons">
                             <?php while( have_rows('social_icons', 'options') ): the_row();
                             $ico_url = get_sub_field('ico_url', 'options');
                             $social_link = get_sub_field('social_link', 'options');
                             ?>
                            <a class="icon" href="<?php echo $social_link; ?>" style="background-image: url(<?php echo $ico_url['url']; ?>)"></a>
                                    <?php endwhile; ?>
                        </div>
                        <?php endif; ?>
                    <?php get_search_form(); ?>
                        <div class="switch-container">
                            <?php
                            $i=0;
                            $languages = pll_the_languages(array('raw'=>1)); 
                            echo '<div id="dd" class="wrapper-dropdown-5" tabindex="1">';
                            echo '<ul class="dropdown">';
                            foreach ($languages as $language){
                              $i++;
                              if ($language['current_lang'])
                                print_r('<li class="current-lang"><img src="'.$language[flag].'"/></li>');
                              else
                                print_r('<li><a href="'.$language[url].'" class="lang-check"><img src="'.$language[flag].'"/></a></li>');
                            }
                            echo '</ul>';
                            echo '</div>';
                            if(!is_page_template('about.php')){
                                pll_default_language($value);
                            }
                            ?>
                        </div>
                    </div>
		<nav id="site-navigation" class="main-navigation">
			<?php
				wp_nav_menu( array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
				) );
			?>
		</nav><!-- #site-navigation -->
            </div>
	</header><!-- #masthead -->
<?php
if (is_front_page()){
	$args = array( 'numberposts' => '1' );
	$recent_posts = wp_get_recent_posts( $args );
	foreach( $recent_posts as $recent ){ ?>
        <div class="full-width-container" style="background-image: url(<?php echo get_the_post_thumbnail_url ($recent["ID"]); ?>);"><div class="inner-container"><div class="recent-attachments"><div class="recent-date"><?php echo get_the_date('d.m.Y'); ?></div> <div class="recent-author"> <?php echo get_the_author_meta('display_name', $recent["post_author"]); ?></div> <h1 class="recent-title"><a href="<?php echo get_permalink($recent["ID"])?>"><?php echo $recent["post_title"]; ?></h1></a><button class="recent-button"><a href="<?php echo get_permalink($recent["ID"]); ?>">Читати</a></button></div></div></div>
        <?php }
}
?>
	<div id="content" class="site-content">
