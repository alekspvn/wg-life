<?php
/**
 * wg-life functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package wg-life
 */
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page();
	
}
if( function_exists('acf_set_options_page_title') )
{
    acf_set_options_page_title( __('Theme Options') );
}
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');
if ( ! function_exists( 'wg_life_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function wg_life_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on wg-life, use a find and replace
		 * to change 'wg-life' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'wg-life', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'wg-life' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'wg_life_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'wg_life_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function wg_life_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'wg_life_content_width', 640 );
}
add_action( 'after_setup_theme', 'wg_life_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function wg_life_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'wg-life' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'wg-life' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'wg_life_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function wg_life_scripts() {
        
	wp_enqueue_style( 'wg-life-style', get_stylesheet_uri() );

	wp_enqueue_script( 'wg-life-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	
        wp_enqueue_script( 'wg-life-common', get_template_directory_uri() . '/js/common.js');
        
        wp_enqueue_script( 'wg-life-zoom-gallery', get_template_directory_uri() . '/js/zoom-gallery.js');
        
        wp_enqueue_script( 'wg-life-jssocials', get_template_directory_uri() . '/js/jssocials.min.js');
        
        wp_enqueue_script( 'wg-life-slick.min', get_template_directory_uri() . '/js/slick.min.js');
        

	wp_enqueue_script( 'wg-life-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
	
        wp_enqueue_style( 'wg-life-bootstrap', get_template_directory_uri() . '/css/bootstrap.css');
        
        wp_enqueue_style( 'wg-life-slick', get_template_directory_uri() . '/css/slick.css');
        
         
        
        wp_enqueue_style( 'wg-life-styles', get_template_directory_uri() . '/css/style.css');
        
        wp_enqueue_style( 'wg-life-jssocials-flat', get_template_directory_uri() . '/css/jssocials-theme-flat.css');
        
        wp_enqueue_style( 'wg-life-jssocials', get_template_directory_uri() . '/css/jssocials.css');
        
        wp_enqueue_style( 'wg-life-magnific-popup', get_template_directory_uri() . '/css/magnific-popup.css');
        
        wp_enqueue_style( 'wg-life-normalize', get_template_directory_uri() . '/css/normalize.css');
	
        wp_enqueue_style( 'wg-life-font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css');
        
        if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'wg_life_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

add_filter( 'get_search_form', 'my_search_form' );
function my_search_form( $form ) {

	$form = '
	<form role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
		<label class="screen-reader-text" for="s">Запрос для поиска:</label>
		<input type="text" placeholder="Пошук по сайту" value="' . get_search_query() . '" name="s" id="s" />
	</form>';

	return $form;
}
// create static cpt
 	 $arg = array(
		'post_type'      => 'statistika',
		'order'          => 'ASC',
		'orderby'        => 'menu_order',
		'posts_per_page' => - 1
	);
	$statistic    = new WP_Query( $arg );
        
add_filter( 'pll_get_post_types', 'remove_post_translation_from_pll', 10, 2 );
function remove_post_translation_from_pll( $post_types, $is_settings ) {
unset( $post_types['post']);
return $post_types;
}

add_filter( 'pll_get_taxonomies', 'remove_taxonomy_translation_from_pll', 10, 2 );
 
function remove_taxonomy_translation_from_pll( $taxonomies, $is_settings ) {
        unset( $taxonomies['category'] );
    return $taxonomies;
}