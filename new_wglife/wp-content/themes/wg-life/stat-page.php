<?php
/*
Template Name: Statistic
*/

get_header(); ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
                    <?php $stat_bg = get_field('stat_bg','options'); 
                    if( !empty($stat_bg) ): ?>
                    <div class="full-container" style="background-image: url('<?php echo $stat_bg['url']; ?>')">
                        <div class="overlay"></div>
                        <div class="container">
                            <h1 class="main-title"><?php the_title(); ?></h1>
                        </div>
                    </div>
                    <?php endif; ?>
                    <div class="container breadcrumb">
                        <?php  if(function_exists('bcn_display')) {
                            bcn_display();
                        }?>
                    </div>
                    <?php if ( $statistic->have_posts() ) : ?>
                    <div class="container">
                        <div class="row">
                        <?php while ( $statistic->have_posts() ) : $statistic->the_post(); ?>
                            <a href="<?php the_permalink(); ?>" class="col-lg-3 col-md-3 col-sm-4 col-xs-12  stat-item-link">
                                <div class="stat-cont">
                                    <div class="statistic-item">
                                        <div class="title-stat-item"><?php the_title(); ?></div>
                                    </div>
                                </div>
                            </a>
                        <?php endwhile; ?>
                        </div>
                    </div>    
                    <?php endif; ?>
                    
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
