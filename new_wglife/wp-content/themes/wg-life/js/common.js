jQuery(document).ready(function($){
       if ($('#back-to-top').length) {
        var scrollTrigger = 100, // px
            backToTop = function () {
                var scrollTop = $(window).scrollTop();
                if (scrollTop > scrollTrigger) {
                    $('#back-to-top').addClass('show');
                } else {
                    $('#back-to-top').removeClass('show');
                }
            };
        backToTop();
        $(window).on('scroll', function () {
            backToTop();
        });
        $('#back-to-top').on('click', function (e) {
            e.preventDefault();
            $('html,body').animate({
                scrollTop: 0
            }, 700);
        });
    }
    // SLICK SLIDER
    $('.slider-post-images').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 3,
  appendArrows: $(".slider-post-images "),
   prevArrow: '<button type="button" class="slick-prev"><</button>',
   nextArrow: '<button type="button" class="slick-next">></button>',
     responsive: [
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        infinite: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});
});

//magnific zoom gallery
jQuery(document).ready(function($) {
	$('.zoom-gallery').magnificPopup({
		delegate: 'a',
		type: 'image',
		closeOnContentClick: false,
		closeBtnInside: false,
		mainClass: 'mfp-with-zoom mfp-img-mobile',
		image: {
			verticalFit: true,
			titleSrc: function(item) {
				return item.el.attr('title') + ' &middot; <a class="image-source-link" href="'+item.el.attr('data-source')+'" target="_blank">image source</a>';
			}
		},
		gallery: {
			enabled: true
		},
		zoom: {
			enabled: true,
			duration: 300, // don't foget to change the duration also in CSS
			opener: function(element) {
				return element.find('img');
			}
		}
		
	});
$("#shareRoundIcons").jsSocials({
  showLabel: false,
    shareIn: "popup",
    shares: [{
        share: "facebook",
        logo: "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjwhRE9DVFlQRSBzdmcgIFBVQkxJQyAnLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4nICAnaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkJz48c3ZnIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDU1MCA1NTAiIGlkPSJMYXllcl8xIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCA1NTAgNTUwIiB4bWw6c3BhY2U9InByZXNlcnZlIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIj48Zz48Zz48Y2lyY2xlIGN4PSIyNzUiIGN5PSIyNzUiIGZpbGw9IiMzRjY1QTYiIHI9IjI1NiIvPjxwYXRoIGQ9Ik0yMzYuMSwxOTAuOGMwLDcuNCwwLDQwLjQsMCw0MC40aC0yOS42djQ5LjRoMjkuNlY0MTZoNjAuOFYyODAuNWg0MC44ICAgICBjMCwwLDMuOC0yMy43LDUuNy00OS42Yy01LjMsMC00Ni4yLDAtNDYuMiwwczAtMjguNywwLTMzLjhjMC01LDYuNi0xMS44LDEzLjItMTEuOGM2LjUsMCwyMC4zLDAsMzMuMSwwYzAtNi43LDAtMzAsMC01MS40ICAgICBjLTE3LjEsMC0zNi41LDAtNDUsMEMyMzQuNiwxMzQsMjM2LjEsMTgzLjQsMjM2LjEsMTkwLjh6IiBmaWxsPSIjRkZGRkZGIiBpZD0iRmFjZWJvb2tfNF8iLz48L2c+PC9nPjwvc3ZnPg=="
            }, {
        share: "twitter",
        logo: "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjwhRE9DVFlQRSBzdmcgIFBVQkxJQyAnLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4nICAnaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkJz48c3ZnIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDQ4IDQ4IiBpZD0iTGF5ZXJfMSIgdmVyc2lvbj0iMS4xIiB2aWV3Qm94PSIwIDAgNDggNDgiIHhtbDpzcGFjZT0icHJlc2VydmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiPjxjaXJjbGUgY3g9IjI0IiBjeT0iMjQiIGZpbGw9IiMxQ0I3RUIiIHI9IjI0Ii8+PGc+PGc+PHBhdGggZD0iTTM2LjgsMTUuNGMtMC45LDAuNS0yLDAuOC0zLDAuOWMxLjEtMC43LDEuOS0xLjgsMi4zLTMuMWMtMSwwLjYtMi4xLDEuMS0zLjQsMS40Yy0xLTEuMS0yLjMtMS44LTMuOC0xLjggICAgYy0yLjksMC01LjMsMi41LTUuMyw1LjdjMCwwLjQsMCwwLjksMC4xLDEuM2MtNC40LTAuMi04LjMtMi41LTEwLjktNS45Yy0wLjUsMC44LTAuNywxLjgtMC43LDIuOWMwLDIsMC45LDMuNywyLjMsNC43ICAgIGMtMC45LDAtMS43LTAuMy0yLjQtMC43YzAsMCwwLDAuMSwwLDAuMWMwLDIuNywxLjgsNSw0LjIsNS42Yy0wLjQsMC4xLTAuOSwwLjItMS40LDAuMmMtMC4zLDAtMC43LDAtMS0wLjEgICAgYzAuNywyLjMsMi42LDMuOSw0LjksMy45Yy0xLjgsMS41LTQuMSwyLjQtNi41LDIuNGMtMC40LDAtMC44LDAtMS4zLTAuMWMyLjMsMS42LDUuMSwyLjYsOC4xLDIuNmM5LjcsMCwxNS04LjYsMTUtMTYuMSAgICBjMC0wLjIsMC0wLjUsMC0wLjdDMzUuMiwxNy42LDM2LjEsMTYuNiwzNi44LDE1LjR6IiBmaWxsPSIjRkZGRkZGIi8+PC9nPjwvZz48L3N2Zz4="
            }, {
        share: "googleplus",
        logo: "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjwhRE9DVFlQRSBzdmcgIFBVQkxJQyAnLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4nICAnaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkJz48c3ZnIGlkPSJDYXBhXzEiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDExMi4xOTYgMTEyLjE5NjsiIHZlcnNpb249IjEuMSIgdmlld0JveD0iMCAwIDExMi4xOTYgMTEyLjE5NiIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+PGc+PGc+PGNpcmNsZSBjeD0iNTYuMDk4IiBjeT0iNTYuMDk3IiBpZD0iWE1MSURfMzBfIiByPSI1Ni4wOTgiIHN0eWxlPSJmaWxsOiNEQzRFNDE7Ii8+PC9nPjxnPjxwYXRoIGQ9Ik0xOS41MzEsNTguNjA4Yy0wLjE5OSw5LjY1Miw2LjQ0OSwxOC44NjMsMTUuNTk0LDIxLjg2N2M4LjYxNCwyLjg5NCwxOS4yMDUsMC43MjksMjQuOTM3LTYuNjQ4ICAgIGM0LjE4NS01LjE2OSw1LjEzNi0xMi4wNiw0LjY4My0xOC40OThjLTcuMzc3LTAuMDY2LTE0Ljc1NC0wLjA0NC0yMi4xMi0wLjAzM2MtMC4wMTIsMi42MjgsMCw1LjI0NiwwLjAxMSw3Ljg3NCAgICBjNC40MTcsMC4xMjIsOC44MzUsMC4wNjYsMTMuMjUyLDAuMTU1Yy0xLjExNSwzLjgyMS0zLjY1NSw3LjM3Ny03LjUxLDguNzU3Yy03LjQ0MywzLjI4LTE2Ljk0LTEuMDA1LTE5LjI4Mi04LjgxMyAgICBjLTIuODI3LTcuNDc3LDEuODAxLTE2LjUsOS40NDItMTguNjc1YzQuNzM4LTEuNjY3LDkuNjE5LDAuMjEsMTMuNjczLDIuNjczYzIuMDU0LTEuOTIyLDMuOTc2LTMuOTc2LDUuODY0LTYuMDUyICAgIGMtNC42MDYtMy44NTQtMTAuNTI1LTYuMjE3LTE2LjYxLTUuNjk4QzI5LjUyNiwzNS42NTksMTkuMDc4LDQ2LjY4MSwxOS41MzEsNTguNjA4eiIgc3R5bGU9ImZpbGw6I0RDNEU0MTsiLz48cGF0aCBkPSJNNzkuMTAyLDQ4LjY2OGMtMC4wMjIsMi4xOTgtMC4wNDUsNC40MDctMC4wNTYsNi42MDRjLTIuMjA5LDAuMDIyLTQuNDA2LDAuMDMzLTYuNjA0LDAuMDQ0ICAgIGMwLDIuMTk4LDAsNC4zODQsMCw2LjU4MmMyLjE5OCwwLjAxMSw0LjQwNywwLjAyMiw2LjYwNCwwLjA0NWMwLjAyMiwyLjE5OCwwLjAyMiw0LjM5NSwwLjA0NCw2LjYwNGMyLjE4NywwLDQuMzg1LTAuMDExLDYuNTgyLDAgICAgYzAuMDEyLTIuMjA5LDAuMDIyLTQuNDA2LDAuMDQ1LTYuNjE1YzIuMTk3LTAuMDExLDQuNDA2LTAuMDIyLDYuNjA0LTAuMDMzYzAtMi4xOTgsMC00LjM4NCwwLTYuNTgyICAgIGMtMi4xOTctMC4wMTEtNC40MDYtMC4wMjItNi42MDQtMC4wNDRjLTAuMDEyLTIuMTk4LTAuMDMzLTQuNDA3LTAuMDQ1LTYuNjA0QzgzLjQ3NSw0OC42NjgsODEuMjg4LDQ4LjY2OCw3OS4xMDIsNDguNjY4eiIgc3R5bGU9ImZpbGw6I0RDNEU0MTsiLz48Zz48cGF0aCBkPSJNMTkuNTMxLDU4LjYwOGMtMC40NTMtMTEuOTI3LDkuOTk0LTIyLjk0OSwyMS45MzMtMjMuMDkyYzYuMDg1LTAuNTE5LDEyLjAwNSwxLjg0NCwxNi42MSw1LjY5OCAgICAgYy0xLjg4OSwyLjA3Ny0zLjgxMSw0LjEzLTUuODY0LDYuMDUyYy00LjA1NC0yLjQ2My04LjkzNS00LjM0LTEzLjY3My0yLjY3M2MtNy42NDIsMi4xNzYtMTIuMjcsMTEuMTk5LTkuNDQyLDE4LjY3NSAgICAgYzIuMzQyLDcuODA4LDExLjgzOSwxMi4wOTMsMTkuMjgyLDguODEzYzMuODU0LTEuMzgsNi4zOTUtNC45MzYsNy41MS04Ljc1N2MtNC40MTctMC4wODgtOC44MzUtMC4wMzMtMTMuMjUyLTAuMTU1ICAgICBjLTAuMDExLTIuNjI4LTAuMDIyLTUuMjQ2LTAuMDExLTcuODc0YzcuMzY2LTAuMDExLDE0Ljc0My0wLjAzMywyMi4xMiwwLjAzM2MwLjQ1Myw2LjQzOS0wLjQ5NywxMy4zMy00LjY4MywxOC40OTggICAgIGMtNS43MzIsNy4zNzctMTYuMzIyLDkuNTQyLTI0LjkzNyw2LjY0OEMyNS45ODEsNzcuNDcxLDE5LjMzMiw2OC4yNiwxOS41MzEsNTguNjA4eiIgc3R5bGU9ImZpbGw6I0ZGRkZGRjsiLz48cGF0aCBkPSJNNzkuMTAyLDQ4LjY2OGMyLjE4NywwLDQuMzczLDAsNi41NywwYzAuMDEyLDIuMTk4LDAuMDMzLDQuNDA3LDAuMDQ1LDYuNjA0ICAgICBjMi4xOTcsMC4wMjIsNC40MDYsMC4wMzMsNi42MDQsMC4wNDRjMCwyLjE5OCwwLDQuMzg0LDAsNi41ODJjLTIuMTk3LDAuMDExLTQuNDA2LDAuMDIyLTYuNjA0LDAuMDMzICAgICBjLTAuMDIyLDIuMjA5LTAuMDMzLDQuNDA2LTAuMDQ1LDYuNjE1Yy0yLjE5Ny0wLjAxMS00LjM5NiwwLTYuNTgyLDBjLTAuMDIxLTIuMjA5LTAuMDIxLTQuNDA2LTAuMDQ0LTYuNjA0ICAgICBjLTIuMTk3LTAuMDIzLTQuNDA2LTAuMDMzLTYuNjA0LTAuMDQ1YzAtMi4xOTgsMC00LjM4NCwwLTYuNTgyYzIuMTk4LTAuMDExLDQuMzk2LTAuMDIyLDYuNjA0LTAuMDQ0ICAgICBDNzkuMDU3LDUzLjA3NSw3OS4wNzksNTAuODY2LDc5LjEwMiw0OC42Njh6IiBzdHlsZT0iZmlsbDojRkZGRkZGOyIvPjwvZz48L2c+PC9nPjxnLz48Zy8+PGcvPjxnLz48Zy8+PGcvPjxnLz48Zy8+PGcvPjxnLz48Zy8+PGcvPjxnLz48Zy8+PGcvPjwvc3ZnPg=="
           }]
});
        //dropdown language switcher
  		function DropDown(el) {
				this.dd = el;
				this.initEvents();
			}
			DropDown.prototype = {
				initEvents : function() {
					var obj = this;

					obj.dd.on('click', function(event){
						$(this).toggleClass('active');
						event.stopPropagation();
					});	
				}
			}

			$(function() {

				var dd = new DropDown( $('#dd') );

				$(document).click(function() {
					// all dropdowns
					$('.wrapper-dropdown-5').removeClass('active');
				});

			});
                        //append object
   $('.current-lang').appendTo($('.wrapper-dropdown-5')); 
       $('.lang-check').click(function(){
          var url = "https://www.rapidtables.com/web/dev/jquery-redirect.htm";
          $(location).attr("href", url); 
      });

});