<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package wg-life
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
            <a href="#" id="back-to-top" title="Back to top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
            <div class="container">
                     <?php $logo_footer = get_field('logo_footer','options'); ?>
            <?php if( !empty($logo_footer) ): ?>
                <div class="footer-slog">
                            <div class="logo-slog">
		<div class="site-branding">


                    <img src="<?php echo $logo_footer['url']; ?>" alt="<?php echo $logo_footer['alt']; ?>" class="custom-logo" />

		</div><!-- .site-branding -->
        <?php endif; ?>
                <div class="logo-text">
                    <p class="bold-text">Загальнофанатський ресурс ФК Ворскла</p>
                    <p>Події, новини з життя клубу та фанатів</p>
                </div>
            </div>
        </div>
		<div class="site-info">
	<nav id="site-navigation" class="main-navigation">
			<?php
				wp_nav_menu( array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
				) );
			?>
		</nav><!-- #site-navigation -->
                <?php if( have_rows('social_icons','options') ): ?>
                        <div class="social-icons">
                             <?php while( have_rows('social_icons', 'options') ): the_row();
                             $ico_url = get_sub_field('ico_url', 'options');
                             $social_link = get_sub_field('social_link', 'options');
                             ?>
                            <a class="icon" href="<?php echo $social_link; ?>" style="background-image: url(<?php echo $ico_url['url']; ?>)"></a>
                                    <?php endwhile; ?>
                        </div>
                        <?php endif; ?>
		</div><!-- .site-info -->
            </div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
</body>
</html>
