<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package wg-life
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
                    <?php if (is_front_page()) { ?>
                    <div class="container">
                        <h2 class="main-title">Останні новини</h2>
                        <div class="row">
                            <?php echo do_shortcode('[ajax_load_more post_type="post" container_type="ul" posts_per_page="9" scroll="false" button_label="Більше новин" button_loading_label="Завантаження..."]'); ?>
                        </div>
                    </div>
                    <?php } 

                    while ( have_posts() ) : the_post(); ?>
                    <?php if(!is_front_page()){ ?>
                    <div class="container">
                        <h2 class="main-title"><?php the_title(); ?></h2>
                        <?php the_content(); ?>
                    </div>
                    <?php } ?>
                    <?php endwhile; ?>
                   <?php if(is_page( 165 )) {
                   if( have_rows('add_pdf') ): ?>
                        <div class="container">
                    <?php while( have_rows('add_pdf') ): the_row();
                    $pdf_title = get_sub_field('pdf_title');
                    $fanzini_media = get_sub_field('fanzini_media');
                    $thumb_pdf = get_sub_field('thumb_pdf');
                    ?>
                            <div class="fanzini-wrapper col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <h4 class="fanzini-title"><?php echo $pdf_title; ?></h4>
                                <a target="blank" href="<?php echo $fanzini_media; ?>">
                                    <div class="fanzini-body">
                                        <img src="<?php echo $thumb_pdf['url'];?>" alt="<?php echo $thumb_pdf['alt'];?>"/>
                                    </div>
                                </a>
                            </div>
                   <?php endwhile; ?>
                        </div>
                    <?php endif; ?>
                  <?php  } ?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
