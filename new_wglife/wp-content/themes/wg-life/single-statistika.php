<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
                    <?php $stat_bg = get_field('stat_bg','options'); 
                    if( !empty($stat_bg) ): ?>
                    <div class="full-container" style="background-image: url('<?php echo $stat_bg['url']; ?>')">
                        <div class="overlay"></div>
                        <div class="container">
                            <h1 class="main-title"><?php the_title(); ?></h1>
                        </div>
                    </div>
                    <?php endif; ?>
                    <div class="container breadcrumb">
                        <?php  if(function_exists('bcn_display')) {
                            bcn_display();
                        }?>
                    </div>
                    <div class="container">
                        <div class="row">
                                 <?php if( have_rows('statistic_table') ): ?>
                            <div class="container stat-container">
                                <table class="table-content">
                                    <tr class="title-table">
                                        <th>Сезон <?php the_title(); ?></th>
                                    </tr>
                <?php while( have_rows('statistic_table') ): the_row(); 
                $date_table = get_sub_field('date_table');
                $town_table = get_sub_field('town_table');
                $count_table = get_sub_field('count_table');
                ?>
                                    <tr>
                                        <td class="col-table-1"><p><?php echo $date_table;?></p></td>
                                        <td class="col-table-2"><p><?php echo $town_table;?></p></td>
                                        <td class="col-table-3"><p><?php echo $count_table;?></p></td>
                                    </tr>
                <?php endwhile; ?>
                                </table>
                                <div class="back-btn-section">
                                    <a class="back-btn" href="/statistika">Назад до статистики</a>
                                </div>
                 <?php if ( $statistic->have_posts() ) : ?>
                    <ul class="stat-list">
                        <?php while ( $statistic->have_posts() ) : $statistic->the_post(); ?>
                            <a href="<?php the_permalink(); ?>" class="stat-item-link">
                                <li class="stat-cont">
                                    <div class="statistic-item">
                                        <div class="title-stat-item"><?php the_title(); ?></div>
                                    </div>
                                </li>
                            </a>
                        <?php endwhile; ?>
                    </ul>
                    <?php endif; ?>
                            </div>
            <?php endif; ?>
                        </div>
                    </div>    
                    
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
