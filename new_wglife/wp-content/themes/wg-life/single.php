<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package wg-life
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
                <?php $single_bg = get_field('single_bg','options'); 
                    if( !empty($single_bg) ): ?>
                    <div class="full-container" style="background-image: url('<?php echo $single_bg['url']; ?>')">
                        <div class="overlay"></div>
                        <div class="container">
                            <h1 class="main-title"><?php the_title(); ?></h1>
                        </div>
                    </div>
                    <?php endif; ?>
                    <div class="container entry-content">
                         <div class="breadcrumb">
                            <?php  if(function_exists('bcn_display')) {
                                bcn_display();
                            }?>
                         </div>
		<?php
		while ( have_posts() ) : the_post(); ?>
                        <div class="container-single-attachments">
                            <h3 class="post-subtitle"><?php the_title(); ?></h3>
                            <div class="single-post-attachments">
                                <div class="post-author post-attachments">
                                    <span><?php the_author(); ?></span>
                                </div>
                                <div class="data-post post-attachments">
                                    <span><?php the_time("F d, Y"); ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="post-thumbnail">
                            <?php the_post_thumbnail('medium'); ?>
                        </div>
                        <div class="post-content">
                            <?php the_content(); ?>
                            <?php 
                            $slider_gallery = get_field('slider_gallery');
                            if( $slider_gallery ): ?>
                            <div class="slider-post-images zoom-gallery">
                                <?php foreach( $slider_gallery as $images ): ?>
                                <div class="slide-img-wrapp">
                                    <a class="slide-container" href="<?php echo $images['url']; ?>">
                                        <img  src="<?php echo $images['url']; ?>" alt="<?php echo $images['alt']; ?>"/>
                                    </a>    
                                </div>
                                <?php endforeach; ?>
                            </div>
                            <?php endif; ?>
                        </div>
		<?php endwhile; // End of the loop.
		?>
                        <?php comments_template( $file, $separate_comments ); ?>
                        <div class="share-container">
                            <div class="share-title">Поділитися новиною</div>
                            <div id="shareRoundIcons"></div>
                        </div>
                        <div class="container">
            <h2 class="main-title">Інші новини</h2>
            <?php echo do_shortcode('[ajax_load_more post_type="post" container_type="ul" posts_per_page="3" category="recent_post" scroll="false" button_label="Більше новин" button_loading_label="Завантаження..."]'); ?>
        </div>
                    </div>     
 
		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_sidebar();
get_footer();
