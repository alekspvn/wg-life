jQuery(document).ready(function($) {
        $('.wpm-title a.wpm-overlay, .wpm-img a.wpm-overlay').magnificPopup({
                type:'iframe',
                disableOn: function() {
                        if($(window).width() < 600) {
                                return false;
                        }
                        return true;
                }
        });
});
