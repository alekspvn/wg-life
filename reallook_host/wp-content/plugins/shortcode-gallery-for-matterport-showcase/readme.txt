=== WP Matterport Shortcode Gallery Embed ===
Contributors: AntiochInteractive
Tags: matterport, shortcode
Donate link: http://metroplex360.com/
Requires at least: 4.0.0
Tested up to: 4.8
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Embed Matterport 3D Showcase Galleries with cached thumbnails, titles and addresses that open in a lightbox!  Responsive, 1 - 4 columns.

== Description ==
Easily embed Matterport Tours with preview image and a pop-up.  Easily include address information and # of scans sourced directly from your Matterport tours.  Optimized with information caching to make a portfolio page load fast!  Includes a great admin page for refreshing cached information and quick access to your models @ MyMatterport.com.

= Examples =

Single Tour:

`[matterport src="uKDy9xrMRCi"]`

Tour Gallery:

`[matterport cols="3" src="uKDy9xrMRCi,wV4vX743ATF"]`

Embed with Labels:

`[matterport cols="3" src="uKDy9xrMRCi,wV4vX743ATF" showdate="true" address="1" showstats="1"]`

Embed with Parameters:

`[matterport cols="3" src="uKDy9xrMRCi,wV4vX743ATF" qs="1" hhl="1" showtitle="2"]`


= Plugin-Specific Parameters =

**src** (required)

* uKDy9xrMRCi - (Single Tour by ID)
* uKDy9xrMRCi,uKDy9xrMRCi,uKDy9xrMRCi - (Multiple tours by ID separated by commas)
* https://my.matterport.com/show/?m=uKDy9xrMRCi - Full URL (unnecessary, but supported)

**cols**

* 1 (default) - Show tour(s) in single column
* 2, 3, 4 - Show tour(s) in multiple columns

**window**
* NULL (default) - Open tours in an overlay
* _blank - Open tours in a blank tab/window
* customname - Open tours in an embedded iframe (e.g. - <iframe id="customname">)

**title**

* NULL (default) - Source titles from Tour
* "My Tour" - Use shortcode defined title on tour
* "My Tour;My Second Tour;My Third Tour" - Use multiple titles separated by semicolons for multiple tours
* "hidden" - Do not show titles or any information below tour.

**address**

* NULL (default) - Do not show address
* 1 - Show caption beneath thumbnail with Street, City, State ZIP

**showdate**

* NULL (default) - Do not show date.
* true - Show date of upload to my.matterport.com in caption
* modified - Show last modified date in caption

**showstats**

* NULL (default) - Do not show # of Scans
* 1 - Show # of Scans after address in caption

**nocache**

* NULL (default) - Use weserve.nl for thumbnail resize and cache
* 1 - Do not use image caching (useful when you need to change the image)

= Launching 3D Showcase =

**help**

* NULL (default) - Help is shown on first visit only
* 0 - Do not show help at all
* 1 - Always show help when the Matterport Space opens.
* 2 - Always show help when the Matterport Space opens.  More concise help text than help=1.

**hl**

* 0 - Briefly show the highlight reel upon launch before hiding. (default if reel has no 360 views)
* 1 - Keep the highlight reel visible upon launch. (default if reel does have 360 views)

**hhl**

* 0 (default) - Show Highlight Reel
* 1 - Hide Highlight Reel

**qs**

* 0 (default) - Disable Quickstart (when the Matterport Space first opens, zoom in from Dollhouse View)
* 1 - Enable Quickstart (when the Matterport Space first opens, go straight into Inside View). <a href="https://support.matterport.com/hc/en-us/articles/233088368" target="_blank">Learn more</a>. Only works if Start Position is Inside View.

**ts**

* -1 (default) - Number of seconds after initial fly-in before the Guided Tour automatically starts. Help is not shown. Set to a negative number to not start the Guided Tour.

= Visibility = 

**brand**

* 0 - Hide 'Presented By' details when Space opens. Hide contact details in the About Panel (top-left grey corner of 3D Showcase).
* 1 (default) - Display all branding information in the About Panel (top-left corner of 3D Showcase).

**mls**

* 0 (default) - Show branding information, links in Mattertag™ Posts, and VR.
* 1 - Show the MLS-friendly version of the Matterport Space. This removes branding, removes links in Mattertag™ Posts, and removes VR.

**showtitle** (title)

* 0	- Hide Title and Title Container
* 1 (Default) - Display Model Title when UI is visible (Default)
* 2	- Always Display Model Title 

= User Interface =

**f**

* 0 - Remove the option to switch floors. This treats the Matterport Space as "all one floor".
* 1 (default) - Let the user navigate the Space floor by floor.

**lang**

* en (default) - Shows 3D Showcase UI in American English
* es - Shows 3D Showcase UI in Spanish
* fr - Shows 3D Showcase UI in French

**wh**

* 0 - Ignore scroll wheel input from the mouse. Scroll wheel input will cause the entire webpage to move up and down. This parameter is only valid when the Space is embedded with an iframe.
* 1 (default) - Enables scroll wheel input from the mouse, so the Space can zoom in. 3D Showcase will only listen to scroll wheel input if the cursor is over the iframe.

= Zooming =

**nozoom**

* NULL (default) - Enable zooming in 3D Showcase
* 1 - Disable zooming in 3D Showcase

**maxzoom**

* 2 (default - 200%) - Set maximum zoom

**minzoom**

* 1 (default - 100%) - Set minimum zoom

**zoomtrans**

* 1 (default) - Smooth Zoom
* 2 - Experimental Zoom Effect                    

= Guided Tours =

**guides**

* 0 - Do not show the path to the next highlight.
* 1 (default) - For Guided Tours with <a href="https://support.matterport.com/hc/en-us/articles/212625138-3-Choose-Tour-Type-Publish" target="_blank">walkthrough transitions</a>, show the blue path on the ground to the next highlight. 

**kb**

* 0 - Do not pan when you reach a new highlight in a Guided Tour.
* 1 (default) - Gently pan once you reach a new highlight in a Guided Tour.

**lp**

* 0 (default) - Stop once you reach the end of the Guided Tour.
* 1 - Loop back to the beginning once you reach the end.

**mf**

* 0 - For Guided Tours with walkthrough transitions, use the 3D mesh when transitioning between highlights.
* 1 (default) - Use 2D panorama imagery during transitions (mesh-free transitions).

**st**

* 3500 (default) - Number of milliseconds to wait at each highlight during a guided tour.
	
**tourcta**

* 0 - No call to action at the end of a a Guided Tour
* 1	(default) - Large call to action at the end of a Guided Tour
* 2	- Small call to action at the end of a Guided Tour

= Virtual Reality =

**vr**

* 0 - Hide the VR button
* 1 (default) - Show the VR button

**vrcoll**

* 0 (default) - Opens the Space in the "Shared with Me" folder within the app. Users can navigate to other Spaces in the Matterport VR app.
* 1 - Opens the Space in a limited mode by itself. Users cannot navigate to other Spaces in the Matterport VR app. This is the same limited mode that is used in VR Collections. 

= Advanced Parameters =

**start**

* NULL (default) - Zoom in from dollhouse
* Custom start point -- hit 'U' while viewing a tour to get this link and copy the 'start' parameter.

== Installation ==
1. Upload the plugin files to the `/wp-content/plugins/wp-matterport-shortcode` directory, or install the plugin 
through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress
3. Use the [matterport] shortcode within any post or page on your site!

== Frequently Asked Questions ==

**Q: I've changed the title of my tour in my.matterport.com, but it won't change on my site!**
A: Visit Admin > Settings > WP Matterport Shortcode to refresh!

**Q: I love the lightbox thing.**
A: Thanks, it's Magnific Popup by Dmitry Semenov.  Go check out his site.  He makes some awesome stuff! 
http://dimsemenov.com/plugins/magnific-popup/

**Q: I'm already using Magnific Popup on my site and this plugin is causing issues.**
A:  wp_dequeue_script('magnific'); wp_dequeue_style('magnific');

**Q: Can you add feature X... Y... Z?**
A: If you think of a good feature, shoot me an e-mail :)

== Screenshots ==
1. Sample Output.
2. Sample of Shortcode in Editor.

== Changelog ==

= 1.6.8 -
* Fixed window parameter again... sorry, seems like the overlay broke.

= 1.6.7 =
* Fixed mls and title parameters
* Fixed 'undefined variable' bug when clearing admin cache.

= 1.6.6 = 
* Added 'nocache' parameter to bypass image caching for when you change the default thumbnail and it won't refresh.

= 1.6.5 = 
* Fixed window parameter... because coding and not testing is stupid.

= 1.6.4 =
* Added 'window' parameter to allow users to open tours in a new window or existing embedded iframe

= 1.6.3 =
* Faster loading images fixed.  Found a much easier method to seve from images.weserv.nl

= 1.6.1 =
* Faster loading images -- LINUX INSTALLS ONLY
* Now uses images.weserv.nl for image resizing and caching -- LINUX APACHE ONLY

= 1.5 =
* Now supports all parameters from https://support.matterport.com/hc/en-us/articles/209980967-URL-Parameters
* Added hhl, minzoom, maxzoom and zoomtrans
* Removed 'unbranded' parameter.  Please use 'brand'
* Removed 'floors' parameter.  Please use 'f'

= 1.4.11 =
* Added 'lang' parameter

= 1.4.10 =
* Added 'nozoom' parameter
* Removed 'VR Ready' Tag as all tours contain CoreVR
* Fixed 'start' parameter and limited to only work on single tour embeds.

= 1.4.9 =
* Fixed bug that was showing all tours unbranded.  Sorry about that.

= 1.4.8 =
* Fixed errors when displaying addresses
* Added QuickStart parameter (qs=1)
* Removed floors parameter
* Removed OpenGraph code (unnecessary)
* Changed unbranded parameter to brand 

= 1.4.7 = 
* Fixed error message caused by meta data change to address.

= 1.4.6 =
* Forced use of OpenMetaGraph tags for images after plugin broke.

= 1.4.5 =
* Tour will skip Lightbox when window is < 600px. (mobile)

= 1.4.4 =
* Fixed non-dollhouse thumbnail generation (broken in 1.4.3)

= 1.4.3 =
* Added support for showing default generated dollhouse thumbnail

= 1.4.2 =
* Added !important to lightbox max-width 1200px

= 1.4.1 =
* 'VR Ready' status appears on tours enhanced for Samsung VR
* Added showdate parameter (false/true/modified)
* Admin - Cached Tour Information displays by date created
* FIX - Duplicate database entries no longer generated on tours with short titles.

= 1.4 = 
* WPMS now saves and retrieves cached data to custom database table - wpms
* Admin - Moved to Top Level with a recognizable icon
* Admin - Plugins Page - Added Link to 'Settings / Cache Management'
* Admin - Added Screen Options - Columns 1,2,3,4 for fun!
* CSS Fix: Forced 'clear: both' to new rows in 2, 3 and 4 column layouts

= 1.3.1 =
* Added title="hidden" option to hide title area.

= 1.3 =
* Added WP Matterport Shortcode Admin Page
* Added ability to refresh data via Admin
* Added ability to quickly access model on MyMatterport.com via Admin
* Removed expiration on cached data.
* Fixed address="true" parameter.  Addresses now display.
* Added margin-bottom: 0 to .wpm img -- to override themes adding space below image.

= 1.2.1 =
* Automatically enqueues jQuery if not included by theme.
* Separated Magnific Pop-Up Javascript/CSS, enqueued as 'magnific' - so that it can be disabled via theme -- if perhaps one uses WF Magnific or IW Magnific plugins.
* Changed line-height to 56px for Arrow -- tnx IBAdvantage

= 1.2 =
* Title and Image URL cached as transients - much faster!
* Added address option: address=TRUE to show street/city/state/zip
* Added showStats option: showStats=TRUE to show XXX Scans
* Fixed source image sizes to 960 width in 1 Column Mode; 640x320 for 2 and higher.
* Added retrieval of Title/Address/# va JSON with OpenGraph fallback
* Changed CSS prefix to wpm- as a universal prefix.
* Updated MagnificPopup to v1.1.0

= 1.1.1 =
* Featured: Added title parameter.  Add a title from the shortcode to single or multiple tours (separate with semi-colon)
* Feature: Added Spanish, French and German internationalization of the ONE line of text -- Explore 3D Space

= 1.0.3 =
* Feature: Added class - 'wpm[matterport ID]' to containers
* Fix: Internet Explorer - Fixed thumbnails by disabling use of Google Image Cache in IE.
* Fix: Added 1em of space below each tour when columns become 100% width (< 740px)
* Optimization: Thumbnail image size set to 600 if using columns (Instead of 900)

= 1.0.1 =
* Added floors, guides help and start parameters.
* Added 1em of margin below shortcode output.

= 1.0 =
* Initial Release.
