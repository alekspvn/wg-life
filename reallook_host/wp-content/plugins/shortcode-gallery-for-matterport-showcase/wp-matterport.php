<?php
/*
 Plugin Name: WP Matterport Shortcode Gallery Embed
 Plugin URI: http://metroplex360.com/wp-matterport
 Description: Embed Matterport 3D Tour Galleries with cached thumbnails and titles that open in a pop-up viewer!  Bundled with Magnific Popup by Dmitry Semenov.
 Version: 1.6.8
 Author: Chris Hickman / Metroplex360.com
 Author URI: http://www.metroplex360.com
 License: GPL2
 Domain Path: /languages
 Text Domain: wp-matterport

 Copyright 2016 Chris Hickman

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License, version 2, as
 published by the Free Software Foundation.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

$wpms_db_version = '1.0';

$installed_ver = get_option( "wpms_db_version" );
if ( $installed_ver != $wpms_db_version ) {
	wpms_install();
	update_option( "wpms_db_version", $wpms_db_version );
}

function wpms_install() {
	global $wpdb;
	global $wpms_db_version;
	$table_name = $wpdb->prefix . 'wpms';
	$charset_collate = $wpdb->get_charset_collate();
	$sql = "CREATE TABLE $table_name (
		id mediumint(9) NOT NULL AUTO_INCREMENT,
		time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
		name tinytext NOT NULL,
		data text NOT NULL,
		UNIQUE KEY id (id)
	) $charset_collate;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );
	add_option( 'wpms_db_version', $wpms_db_version );
}

function myplugin_update_db_check() {
    global $wpms_db_version;
    if ( get_site_option( 'wpms_db_version' ) != $wpms_db_version ) {
        wpms_install();
    }
}
add_action( 'plugins_loaded', 'myplugin_update_db_check' );

register_activation_hook( __FILE__, 'wpms_install' );

function wp_matterport_scripts() {
	wp_enqueue_script('jquery');	
	wp_register_script( 'magnific', plugins_url( 'magnific.min.js', __FILE__ ), array('jquery') );
	wp_enqueue_script( 'magnific' );
 	wp_register_style( 'magnific', plugins_url( 'magnific.css', __FILE__ ) );
	wp_enqueue_style( 'magnific' );
	wp_register_script( 'wp-matterport', plugins_url( 'wp-matterport.js', __FILE__ ), array('jquery','magnific') );
	wp_enqueue_script( 'wp-matterport' );
	wp_register_style( 'wp-matterport', plugins_url( 'wp-matterport.css', __FILE__ ) );
	wp_enqueue_style( 'wp-matterport' );
}
add_action( 'wp_enqueue_scripts', 'wp_matterport_scripts' );

if (is_admin()) {
	function my_enqueue($hook) {
		if ( 'toplevel_page_wpms-options' != $hook ) { return; }
		wp_matterport_scripts(); 
	}
	add_action( 'admin_enqueue_scripts', 'my_enqueue' );
	
	// Add Plugin Settings Links
	function plugin_add_settings_link( $links ) {
	    $settings_link = '<a href="admin.php?page=wpms-options">' . __( 'Settings / Cache Management' ) . '</a>';
	    array_push( $links, $settings_link );
	  	return $links;
	}
	$plugin = plugin_basename( __FILE__ );
	add_filter( "plugin_action_links_$plugin", 'plugin_add_settings_link' );
}	

// Admin
if (is_admin()) {
	include_once('wp-matterport-admin.php');
}

// Add Shortcode
function matterport_embed_shortcode( $atts ) {
	$date_format = get_option('date_format');
	// Attributes
	extract( shortcode_atts(
		array(
			'window' => NULL,
			'address' => NULL,
			'showstats' => NULL,
			'nocache' => NULL,
			'cols' => 1,
			'title' => NULL,
			'guides' => NULL,
			'f' => NULL,
			'help' => NULL,
			'hl' => NULL,
			'hhl' => NULL,
			'kb' => NULL,
			'lp' => NULL,
			'mf' => NULL,
			'st' => NULL,
			'vr' => NULL,
			'vrcoll' => NULL,
			'tourcta' => NULL,
			'qs' => NULL,
			'ts' => NULL,
			'mls' => NULL,
			'wh' => NULL,
			'lang' => NULL,
			'brand' => NULL,
			'showdate' => NULL,
			'src' => '',
			'start' => NULL,
			'admin' => NULL,
			'nozoom' => NULL,
			'maxzoom' => NULL,
			'minzoom' => NULL,
			'zoomtrans' => NULL
		), $atts )
	);
	
	if (substr_count($src,','))
	    $tours = explode(',',$src);
	else
	    $tours = array($src);
	if ($title != NULL)
		if (substr_count($title,';'))
			$titles = explode(';',$title);
		else
			$titles = array($title);
    ob_start();
?>
        <div class="wpm-gallery">
<?php
	$i = 0;
	foreach ($tours as $src) {
		$src = str_replace("https://my.matterport.com/show/?m=", "", $src);
		$link = 'https://my.matterport.com/show/?m=' . $src;

// Launching a 3D Showcase
		if ($help!=NULL && ($help == 1 || $help == 0 || $help == 2))
		    $link .= '&amp;help=' . $help;
		if ($hl!=NULL && ($hl == 1 || $hl == 0))
		    $link .= '&amp;hl=' . $hl;		
		if ($hhl!=NULL && ($hhl == 1 || $hhl == 0)) // Undocumented
		    $link .= '&amp;hhl=' . $hhl;	
		if ($qs!=NULL && ($qs == 0 || $qs == 1))
		    $link .= '&amp;qs=' . $qs;
		if ($ts!=NULL && ($ts > 0 && is_int($ts)))
		    $link .= '&amp;ts=' . $ts;
		// Play omitted

// Visibility
		if ($brand!=NULL && ($brand == 1 || $brand == 0))
		    $link .= '&amp;brand=' . $brand;

		if ($mls!=NULL && ($mls == 1 || $mls == 0))
		    $link .= '&amp;mls=' . $mls;

		// Undocumented
		if ($title!=NULL && ($title == 1 || $title == 0 || $title == 2))
		    $link .= '&amp;title=' . $showtitle;

// User Interface
		if ($f!=NULL && $f == 0)
		    $link .= '&amp;f=' . $f;
		if ($lang != NULL && $lang == 'fr' || $lang == 'es')
		    $link .= '&lang=' . $lang;
		if ($wh!=NULL && ($wh == 1 || $wh == 0))
		    $link .= '&amp;wh=' . $wh;

// Zooming
		if ($nozoom!=NULL && ($nozoom == 1 || $nozoom == 0))
		    $link .= '&nozoom=' . $nozoom;
		if ($maxzoom!=NULL)
		    $link .= '&maxzoom=' . $maxzoom;
		if ($minzoom!=NULL)
		    $link .= '&minzoom=' . $minzoom;
		if ($zoomtrans!=NULL && ($zoomtrans == 1 || $zoomtrans == 0))
		    $link .= '&zoomtrans=' . $zoomtrans;

// Guided Tours
		
		if ($guides!=NULL && $guides == 0)
		    $link .= '&amp;guides=0';
		if ($kb!=NULL && ($kb == 1 || $kb == 0))
		    $link .= '&amp;kb=' . $kb;
		if ($lp!=NULL && ($lp == 1 || $lp == 0))
		    $link .= '&amp;lp=' . $lp;
		if ($mf!=NULL && ($mf == 1 || $mf == 0))
		    $link .= '&amp;mf=' . $mf;
		if ($st!=NULL && ($st > 0 && is_int($st)))
		    $link .= '&amp;st=' . $st;
		if ($tourcta!=NULL && ($tourcta == 1 || $tourcta == 0 || $tourcta == 2))
		    $link .= '&amp;tourcta=' . $tourcta;

// Virtual Reality	
		if ($vr!=NULL && ($vr == 1 || $vr == 0))
		    $link .= '&amp;vr=' . $vr;
		if ($vrcoll!=NULL && ($vrcoll == 1 || $vrcoll == 0))
		    $link .= '&amp;vrcoll=' . $vrcoll;

// Advanced
		if (count($tours) > 1 && $start!=NULL)
		    $link .= '&amp;start=' . $start;

		// Let's see if we have the data cached in the database...
		global $wpdb;
		$query = 'SELECT data from ' . $wpdb->prefix . 'wpms WHERE name="' . $src .'" LIMIT 1';
		$cachedTour = $wpdb->get_var( $query );
		// Pull JSON object from Wordpress Transients as array
		if (strlen($cachedTour) > 1)
			$thisTour = json_decode($cachedTour, TRUE);
		// Otherwise, let's go and find it!
		else {
			// Connect to JSON Feed
			$json = 'https://my.matterport.com/api/v1/player/models/' . $src . '/?format=json';
			if (function_exists("curl_init")) {
				$curl_options = array(
					CURLOPT_URL => $json,
					CURLOPT_HEADER => 0,
					CURLOPT_RETURNTRANSFER => TRUE,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_SSL_VERIFYPEER => 0,
					CURLOPT_ENCODING => 'gzip,deflate',
				);
				$ch = curl_init();
				curl_setopt_array( $ch, $curl_options );
				$output = curl_exec( $ch );
				curl_close($ch);
				$arr = json_decode($output,true);
				$values = array("name","address","contact_name","contact_phone","formatted_contact_phone","contact_email");
				foreach($values as $value)
					$thisTour[$value] = $arr[$value];				
				$thisTour["nodes"] = count($arr['sweeps']);
				$thisTour["player_options"] = $arr['player_options'];
				$thisTour["is_vr"] = $arr['is_vr'];
				$thisTour["created"] = $arr["created"];
				$thisTour["modified"] = $arr["modified"];
			}
			$thisTour["image"] = 'http://my.matterport.com/api/v1/player/models/' . $src . '/thumb';

			// Save data to Wordpress to cache for next time...
			$jsonData = json_encode($thisTour);
			$table_name = $wpdb->prefix . 'wpms';
			$wpdb->insert( $table_name, 
				array( 
					'time' => $arr["created"],
					'name' => $src, 
					'data' => $jsonData 
				) 
			);
		}

		// If title parameter is used in shortcode ...
		if (isset($titles[$i]) && strlen($titles[$i]) > 2) {
			$thisTour["name"] = $titles[$i];
			if (strlen($thisTour["name"]) < 1)
				$thisTour["name"] = "hidden";
		}

		$width = ($cols == 1 ? 960 : 640);
		$height = ($cols == 1 ? 540 : 360);
		$u_agent = $_SERVER['HTTP_USER_AGENT']; 
		if (isset($nocache) && $nocache != NULL)
			$thisTour["image"] = '//my.matterport.com/api/v1/player/models/' . $src . '/thumb/';
		else
			$thisTour["image"] = '//images.weserv.nl/?w=' . $width . '&q=80&url=my.matterport.com/api/v1/player/models/' . $src . '/thumb/';
?>
            <div class="wpm<?php 
	    if ($cols > 1)
        	echo ' wpm-cols' . $cols;
        ?>">
                <div class="wpm-img">
                    <a href="<?php echo $link ?>&amp;play=1" class="<?php echo ($window == NULL ? 'wpm-overlay ' : '')?>wpm-tour wpm<?php echo $src ?>"<?php

		if ($window != NULL)
			echo ' target="' . $window . '"';
		
					?>>
					<img src="<?php echo $thisTour["image"] ?>" />
                        <b>&#9658;</b>
                        <i><?php _e('Explore 3D Space', 'wp-matterport'); ?></i>			
                    </a>
                </div>
                <div class="wpm-info">
<?php
		if ($thisTour["name"] != "hidden") {
?>
                	<span class="wpm-title"><a class="<?php echo ($window ? 'wpm-overlay ' : '')?>wpm-tour" href="<?php echo $link ?>&amp;play=1<?php
					
		if ($window != NULL)
			echo ' target="' . $window . '"';					

					?>"><?php echo $thisTour["name"] ?></a></span>
<?php
		}
		if (isset($thisTour["address"]) && $address!=NULL) {
			if (!is_array($thisTour["address"]))
				$thisAddress = json_decode($thisTour["address"]);
			else
				$thisAddress = (object) $thisTour["address"];
?>
					<div class="wpm-address" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
<?php
			if (strlen($thisAddress->address_1) > 2) {
?>				
						<span class="wpm-street" itemprop="streetAddress"><?php 
			echo $thisAddress->address_1;
			if (strlen($thisAddress->address_2) > 2)
				echo '<span>' . $thisAddress->address_2 . '</span>'; 
						?></span>
<?php
			}
			if (strlen($thisAddress->city) > 2)
				echo "\t\t\t\t\t\t<span class='wpm-city' itemprop='addressLocality'>" . $thisAddress->city . "</span>\n";
			if (strlen($showAddress->state) > 2)
				echo "\t\t\t\t\t\t<span class='wpm-state' itemprop='addressRegion'>" . $thisAddress->state . "</span>\n";
			if (strlen($thisAddress->zip) > 2)
				echo "\t\t\t\t\t\t<span class='wpm-zip' itemprop='postalCode'>" . $thisAddress->zip . "</span>\n";
?>
					</div>
<?php
		}
		if ($showstats!=NULL && isset($thisTour["nodes"])) {
			echo "\t\t\t\t\t<span class=\"wpm-nodes\">\n";
			echo "\t\t\t\t\t\t<span class=\"wpm-scans\">" . $thisTour["nodes"] . " ";
			_e('Scans','wp-matterport');
			echo "</span>\n";
			if ($thisTour["floors"] > 1) {
				echo "\t\t\t\t\t\t<span class=\"wpm-floors\">" . $thisTour["floors"];
				_e('Floors','wp-matterport');
				echo "</span>\n";
			}
			echo "\t\t\t\t\t</span>\n";
		}
		if ($showdate != NULL) {
			if ($showdate == 'modified')
				echo "\t\t\t\t\t" . '<div class="wpm-date">' . __('Last modified','wp-matterport') . ': ' . date($date_format,strtotime($thisTour["modified"])) . '</div>';
			else
				echo '<div class="wpm-date">' . __('Created', 'wpst modified: ','wp-matterport') . ': ' . date($date_format,strtotime($thisTour["created"])) . '</div>';
		}
		echo "\t\t\t\t</div>\n";
		if ($admin != NULL) {
?>
			<div style="text-align: center">
				<a href="<?php echo $_SERVER["PHP_SELF"] ?>?page=wpms-options&amp;refresh=<?php echo $src ?>"><?php _e('Reload Thumbnail / Information','wp-matterport'); ?></a> | 
				<a href="https://my.matterport.com/models/<?php echo $src ?>" target="_blank"><?php _e('MyMatterport','wp-matterport'); ?></a>
			</div>
<?php
		}
		echo "\n\n\n</div>\n";
		$i++;
		}
	echo "\n\n</div>\n";
	return ob_get_clean();
}
add_shortcode( 'matterport', 'matterport_embed_shortcode' );
