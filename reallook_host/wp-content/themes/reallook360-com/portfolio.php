
<?php
/*
Template Name: Portfolio template
*/
?>




          <script>

            jQuery(document).ready(function(){
                             //zoom gallery
    jQuery('.zoom-gallery').magnificPopup({
            delegate: 'a',
            type: 'image',
            closeOnContentClick: false,
            closeBtnInside: false,
            mainClass: 'mfp-with-zoom mfp-img-mobile',
            image: {
                    verticalFit: true,
                    titleSrc: function(item) {
                            return item.el.attr('title') + ' &middot; <a class="image-source-link" href="'+item.el.attr('data-source')+'" target="_blank">image source</a>';
                    }
            },
            gallery: {
                    enabled: true
            },
            zoom: {
                    enabled: true,
                    duration: 300, // don't foget to change the duration also in CSS
                    opener: function(element) {
                            return element.find('img');
                    }
            }

    });
//Tabs
 // tabbed content
    jQuery(".tab_content").hide();
    jQuery(".tab_content:first").show();

  /* if in tab mode */
    jQuery("ul.tabs li").click(function() {

      jQuery(".tab_content").hide();
      var activeTab = jQuery(this).attr("rel");
      jQuery("#"+activeTab).fadeIn(500);

      jQuery("ul.tabs li").removeClass("active");
      jQuery(this).addClass("active");

	  jQuery(".tab_drawer_heading").removeClass("d_active");
	  jQuery(".tab_drawer_heading[rel^='"+activeTab+"']").addClass("d_active");

    });
	/* if in drawer mode */
	jQuery(".tab_drawer_heading").click(function() {

      jQuery(".tab_content").hide();
      var d_activeTab = jQuery(this).attr("rel");
      jQuery("#"+d_activeTab).fadeIn(500);

	  jQuery(".tab_drawer_heading").removeClass("d_active");
      jQuery(this).addClass("d_active");

	  jQuery("ul.tabs li").removeClass("active");
	  jQuery("ul.tabs li[rel^='"+d_activeTab+"']").addClass("active");
    });


	/* Extra class "tab_last"
	   to add border to right side
	   of last tab */
	jQuery('ul.tabs li').last().addClass("tab_last");

        jQuery('ul.works-tab li:first-child').addClass('active');



        // map tabs
        //Tabs
 // tabbed content
    jQuery(".maptab_content").hide();
    jQuery(".maptab_content:first").show();

  /* if in tab mode */
    jQuery("ul.maptabs li").click(function() {

      jQuery(".maptab_content").hide();
      var activeTab = jQuery(this).attr("rel");
      jQuery("#"+activeTab).fadeIn(500);

      jQuery("ul.maptabs li").removeClass("active");
      jQuery(this).addClass("active");

	  jQuery(".maptab_drawer_heading").removeClass("d_active");
	  jQuery(".maptab_drawer_heading[rel^='"+activeTab+"']").addClass("d_active");

    });
	/* if in drawer mode */
	jQuery(".maptab_drawer_heading").click(function() {

      jQuery(".maptab_content").hide();
      var d_activeTab = jQuery(this).attr("rel");
      jQuery("#"+d_activeTab).fadeIn(500);

	  jQuery(".maptab_drawer_heading").removeClass("d_active");
      jQuery(this).addClass("d_active");

	  jQuery("ul.maptabs li").removeClass("active");
	  jQuery("ul.maptabs li[rel^='"+d_activeTab+"']").addClass("active");
    });


	/* Extra class "tab_last"
	   to add border to right side
	   of last tab */
	jQuery('ul.maptabs li').last().addClass("maptab_last");
	jQuery('.areas ul.works-tab li:first-child').addClass('active');

jQuery(document).ready(function(){
  if ('.entry-modal iframe') {
    jQuery(this).css('width','100%');
  }
  else {
      jQuery('.entry-modal a.portfolio-btn').css('margin','0 auto');
  }
  jQuery('.frames-wrapp p').css('display','none');
  var youtube_frame = jQuery('.frames-wrapp iframe[src*="youtube"]');
  jQuery(youtube_frame).unwrap();
  jQuery(youtube_frame).parent().addClass('customize');
 
            });
        });
        </script>
 <?php get_header();?>
<script src="http://www.reallook360.com/wp-content/themes/reallook360-com/js/magnific-popup.js"></script>
                <style>
            .popup-img {
                height: 300px;
                width: 100%;
            }
        </style>
           <div class="container video-section">
      <video autoplay="true" class="head-video" loop="loop">
        <source src="<?php the_field('video_bg_url') ?>" type="video/mp4" loop>
      </video>
      <?php if( get_field('mob_head_bg') ): ?>
      <div class="mob-head-bg" style=" background-image: url(<?php the_field('mob_head_bg'); ?>)">
      </div>
      <?php endif; ?>
      <!--   <?php if( get_field('panorama_head') ): ?>
<div class="head-panorama"><?php the_field('panorama_head'); ?></div>
<?php endif; ?> -->
      <div class="row">
        <div class="main-title">
          <?php if( get_field('first_row') ): ?>
          <div class="title-row first-row">
            <?php the_field('first_row'); ?>
          </div>
          <?php endif; ?>
          <?php if( get_field('second_row') ): ?>
          <div class="title-row second-row">
            <?php the_field('second_row'); ?>
          </div>
          <?php endif; ?>
        </div>
      </div>
    </div>

      
<div class="container portfolio folio-objects">
  <div class="row">
    <?php if( get_field('folio_title') ): ?>
      <h2 class="titles">
        <?php the_field('folio_title'); ?>
    </h2>
<?php endif; ?>
      <?php if ( $portfolio->have_posts() ) : ?>
    <div class="tabs-wrapp">
      <ul class="tabs works-tab folio-tabs">
          <?php while ( $portfolio->have_posts() ) : $portfolio->the_post(); ?>
        <li  rel="tab<?php echo create_slug(the_title()); ?>">
            <?php the_title(); ?>
      </li>
      <?php
      endwhile; ?>
  </ul>
</div>
<?php endif; 
wp_reset_query(); ?>

<?php if ( $portfolio->have_posts() ) : ?>
<div class="tab_container">
    <?php while ( $portfolio->have_posts() ) : $portfolio->the_post(); ?>
                    <?php if( have_rows('select_type') ): ?> <!-- image galler if -->
                      <?php   while ( have_rows('select_type') ) : the_row(); ?>
                       <?php if( get_row_layout() == 'image_gallery' ): ?>
                <div id="tab<?php echo create_slug(the_title()); ?>" class="tab_content"> 
                          <?php if( have_rows('category_of_photo') ): 
                              while( have_rows('category_of_photo') ): the_row();
                           $category_name = get_sub_field('category_name');?>
                    <div class="category-containser">
                        <h3 class="titles"><?php echo $category_name; ?></h3>
                            <?php $photo_gallery = get_sub_field('photo_gallery');
                   if( $photo_gallery ): 
                          foreach( $photo_gallery as $photo_images ): ?>  <!-- gallery images -->
                          <div class="grid-custom col-lg-4 col-md-4 col-sm-6 col-xs-12">
                              <div class="block">
                              <a target="_blank" href="<?php echo $photo_images['sizes']['large']; ?>" class="folio-item" data-gallery>
                                  <img class="popup-img" src="<?php echo $photo_images['sizes']['large']?>" alt="<?php echo $photo_images['alt']; ?>">
                                </a>
                              </div>
                          </div>
                     <?php endforeach; ?>
                    <?php endif; ?>
                    </div>
                          <?php endwhile; ?> <!-- while gallery -->
                          <?php endif;?> <!-- repeater gallery -->
                   
                    </div> 
                      <?php endif; ?> <!-- category image gallery ==========================================-->
                          <?php if( get_row_layout() == 'frame_video_tour' ): ?>
                   <div id="tab<?php echo create_slug(the_title()); ?>" class="tab_content">       
                          <?php if( have_rows('category_of_frame') ): 
                              while( have_rows('category_of_frame') ): the_row();
                           $category_name = get_sub_field('category_name');
                           $frames_list = get_sub_field('frames_list');?>
                    <div class="category-container">
                        <h3 class="titles"><?php echo $category_name; ?></h3>
                            <div class="frames-container">
                                <?php echo $frames_list; ?>
                            </div>
                        </div>
                          <?php endwhile; ?> <!-- while frame -->
                          <?php endif;?> <!-- repeater frame -->
              </div> <!-- teb container -->
                         <?php endif; ?> <!-- category image frames ==========================================-->
              
                          <?php if( get_row_layout() == 'website_link' ): ?>
              <div id="tab<?php echo create_slug(the_title()); ?>" class="tab_content">       
                         <?php  if( have_rows('category_of_website') ): 
                              while( have_rows('category_of_website') ): the_row();
                           $category_name = get_sub_field('create_website_link_group');
                           $gallery_link = get_sub_field('gallery_link'); ?>
                    <div class="category-container">
                        <h3 class="titles"><?php echo $category_name; ?></h3>
                                 <?php if( $gallery_link ): ?>
                               <div class="website-container">
                          <?php foreach( $gallery_link as $links ): ?>
                               <a target="blank" href="<?php echo $links['caption'] ?>">
                                    <img src="<?php echo $links['sizes']['medium']; ?>" alt="<?php echo $links['alt'];?>">
                                </a>
                                   <?php endforeach; ?>
                            </div>
                    <?php endif; ?>
                        </div>
                          <?php endwhile; ?> <!-- while frame -->
                          <?php endif;?> <!-- repeater frame -->
              </div> <!-- teb container -->
                         <?php endif; ?> <!-- category website layout ==========================================-->
                         <?php endwhile; ?> <!-- select type end while -->
                     <?php endif;?> <!-- select type end if -->
    <?php endwhile; ?>
</div>
<?php endif; 
wp_reset_query(); ?>

      </div>
    </div> 
        
        <!-- end portfolio -->
         <!-- contact form -->
    <div class="container cf-7"  id="contact" style="background-image: url(<?php the_field('cf_bg'); ?>);">
      <?php if( get_field('cf_title') ): ?>
      <h2 class="titles">
        <?php the_field('cf_title'); ?>
      </h2>
      <?php endif; ?>
      <div class="row">
        <div class="contact-row">
          <a href="tel:<?php the_field('contact_top_phone'); ?>">
            <i class="fa fa-phone" aria-hidden="true">
            </i>
            <span class="contacts-cf">
              <?php the_field('contact_top_phone'); ?>
            </span>
          </a>
          <span class="line-after"> |
          </span>
          <a href="mailto:<?php the_field('contact_top_email'); ?>">
            <i class="fa fa-envelope" aria-hidden="true">
            </i>
            <span class="contacts-cf">
              <?php the_field('contact_top_email'); ?>
            </span>
          </a>
        </div>
        <?php the_field('cf_short'); ?>
      </div>
    </div>
        
         <?php get_footer(); ?>