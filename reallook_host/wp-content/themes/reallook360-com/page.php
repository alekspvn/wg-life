<?php
   /**
    * The template for displaying all pages
    *
    * This is the template that displays all pages by default.
    * Please note that this is the WordPress construct of pages
    * and that other 'pages' on your WordPress site may use a
    * different template.
    *
    * @link https://codex.wordpress.org/Template_Hierarchy
    *
    * @package reallook360.com
    */
   
   get_header(); ?>
<div id="primary" class="content-area">
   <main id="main" class="site-main">
      <div class="container video-section">
         <video autoplay="true" class="head-video" loop="loop">
            <source src="<?php the_field('video_bg_url') ?>" type="video/mp4" loop>
         </video>
         <?php if( get_field('panorama_head') ): ?>
         <div class="head-panorama"><?php the_field('panorama_head'); ?></div>
         <?php endif; ?>
         <div class="row">
            <div class="main-title">
               <?php if( get_field('first_row') ): ?>
               <div class="title-row first-row"><?php the_field('first_row'); ?></div>
               <?php endif; ?>
               <?php if( get_field('second_row') ): ?>
               <div class="title-row second-row"><?php the_field('second_row'); ?></div>
               <?php endif; ?>
            </div>
         </div>
      </div>
      <!-- What we do -->
      <script>
         jQuery(document).ready(function($) {
         $(window).bind('scroll', function() {
          // The value of where the "scoll" is.
          if($(window).scrollTop() > 44){
            $('.menu-container').addClass('fixed');
          }else{
            $('.menu-container').removeClass('fixed');
            // Adding padding so it doesn't jump up
            // when the menu gets fixed.sdfsdfsdfsdfdf
            $('.container').css('padding-top', '0px');
          }
         });
         });
      </script>

      <div class="container we-do">
         <?php if( get_field('title_do') ): ?>
         <h2 class="titles"> <?php the_field('title_do'); ?></h2>
         <?php endif; ?>
         <div class="row">
            <?php if( have_rows('icons_list') ): 
               $count_modal = 1;
               while( have_rows('icons_list') ): the_row(); 
               $icon_do = get_sub_field('icon_do'); 
               $subtitle_do = get_sub_field('subtitle_do'); 
               $modal_content = get_sub_field('modal_content'); 
               $modal_img = get_sub_field('modal_img'); 
               $modal_title = get_sub_field('modal_title'); 
               ?>
            <a class="icon-container col-lg-2 col-md-2 col-sm-4 col-xs-6" data-toggle="modal" data-target=".bd-pop<?php echo $count_modal; ?>-modal-lg">
               <div class="icon-box"><img src="<?php echo $icon_do['url']; ?>" alt="<?php echo $icon_do['alt']; ?>" /></div>
               <div class="subtitle-icon">
                  <p><?php echo $subtitle_do ?></p>
               </div>
            </a>
            <div class="modal fade <?php echo 'bd-pop'.$count_modal.'-modal-lg';?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
               <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                      <?php if($modal_title): ?> <h3><?php echo $modal_title; ?></h3><?php endif; ?>
                      <?php if($modal_img): ?> <img src="<?php echo $modal_img['url']?>" alt="<?php echo $modal_img['alt']; ?>"><?php endif; ?> 
                      <div class="entry-modal"><?php echo $modal_content; ?></div>
                  </div>
               </div>
            </div>
            <?php
               $count_modal++;
               endwhile; ?>
         </div>
         <?php endif; ?>
      </div>
      
      <!-- TABLE -->
      <div class="container" id="prices">
         <?php if( get_field('prices') ): ?>
         <h2 class="titles"> <?php the_field('prices'); ?></h2>
         <?php endif; ?>
         <?php if( get_field('prices_table') ): ?> 
         <?php the_field('prices_table'); ?>
         <?php endif; ?>
      </div>
      <!-- All in one -->
      <div class="container in-one">
         <?php if( get_field('title_in_one') ): ?>
         <h3 class="titles-in-one"> <?php the_field('title_in_one'); ?></h3>
         <?php endif; ?>
         <div class="row">
         <?php if( have_rows('icons_in_one') ): ?>
         <ul class="in-one-list">
            <?php while( have_rows('icons_in_one') ): the_row(); 
               $icon_in_one = get_sub_field('icon_in_one'); 
               $subtitle_in_one = get_sub_field('subtitle_in_one'); ?>
            <li class="icon-in-one">
               <img src="<?php echo $icon_in_one['url']; ?>" alt="<?php echo $icon_in_one['alt']; ?>">
               <div class="subtitle-in-one">
                  <p><?php echo $subtitle_in_one ?></p>
               </div>
            </li>
            <i class="fa fa-plus" aria-hidden="true"></i>
            <?php endwhile; ?>
         </ul>
         <?php endif; ?>
         </div>
      </div>
      <!-- Our works -->
      <div class="container our-works">
         <div class="row">
            <?php if( get_field('our_works') ): ?>
            <h2 class="titles"> <?php the_field('our_works'); ?></h2>
            <?php endif; ?>
            <?php if( have_rows('create_works') ): 
               $count = 1;
               ?>
            <ul class="tabs works-tab">
               <?php while( have_rows('create_works') ): the_row(); 
                  $tab = get_sub_field('create_tab'); ?>
               <li  rel="tab<?php echo $count; ?>"><?php echo $tab; ?></li>
               <?php 
                  $count++;
                  endwhile; ?>
            </ul>
            <?php endif; ?>      
            <div class="tab_container zoom-gallery">
               <?php if( have_rows('create_works') ): 
                  $count = 1; 
                  while( have_rows('create_works') ): the_row(); 
                  $tab = get_sub_field('create_tab');
                  if( have_rows('create_images') ): ?>
               <div id="tab<?php echo $count; ?>" class="tab_content">
                  <?php $count++; ?>
                  <?php while( have_rows('create_images') ): the_row(); 
                     $object = get_sub_field('create_object'); ?>
                  <a href="<?php echo $object['url']; ?>" class="object-wrapp"><img src="<?php echo $object['url']; ?>" alt="<?php echo $object['alt']; ?>"></a>  
                  <?php    endwhile; ?>
               </div>
               <?php endif; ?>
               <?php endwhile; ?>
               <?php endif; ?>
            </div>
            <a class="portfolio-btn" href="#">View portfolio</a>
         </div>
      </div>
      <!-- From customers -->
      <div class="container slider-t" id="testimonials">
         <?php if( get_field('customers_title') ): ?>
         <h2 class="titles"> <?php the_field('customers_title'); ?></h2>
         <?php endif; ?>
         <?php 
            if( have_rows('t_slider') ):?>
         <div class="testimonial-slider">
            <?php 
               while( have_rows('t_slider') ): the_row(); 
                $t_logo = get_sub_field('t_logo');
                $customer_name = get_sub_field('customer_name');
                $customer_role = get_sub_field('customer_role');
                $testimonial = get_sub_field('testimonial');
                ?>
            <div class="t-container">
               <img class="t-logo" src="<?php echo $t_logo['url']; ?>" alt="<?php echo $t_logo['alt']; ?>">
               <div class="cusomer-name">
                  <p><?php echo $customer_name; ?></p>
               </div>
               <div class="customer-role">
                  <p><?php echo $customer_role; ?></p>
               </div>
               <div class="tes-wrapp">
                    <div class="testimonial"><?php echo $testimonial; ?></div>
               </div>
            </div>
            <?php endwhile; ?>
         </div>
         <?php endif; ?>
      </div>
      <!-- Why us -->
      <div class="container why-us" style="background-image: url('<?php the_field('whyus_bg'); ?>')">
         <?php if( get_field('why_us') ): ?>
         <h2 class="titles"> <?php the_field('why_us'); ?></h2>
         <?php endif; ?>
         <div class="row whyus-row">
            <?php 
               if( have_rows('whyus_icons') ):
               while( have_rows('whyus_icons') ): the_row(); 
                    $icon_whyus = get_sub_field('whyus_item');
                    $subtitle_whyus = get_sub_field('whyus_subtitle');
                    ?>
            <div class="whyus-wrapp col-lg-3 col-md-3 col-sm-6 col-xs-12">
               <div class="why-us-img">
                  <img src="<?php echo $icon_whyus['url']; ?>" alt="<?php echo $icon_whyus['alt']; ?>">
               </div>
               <div class="whyus-subtitle">
                  <p><?php echo $subtitle_whyus; ?></p>
               </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
         </div>
      </div>
      <!-- Areas of operation -->
      <div class="container areas">
        <div class="row">
         <?php if( get_field('areas') ): ?>
         <h2 class="titles"> <?php the_field('areas'); ?></h2>
         <?php endif; ?>
            <?php if( have_rows('create_maps') ): 
               $count = 1;
               ?>
            <ul class="maptabs works-tab">
               <?php while( have_rows('create_maps') ): the_row(); 
                  $maptab = get_sub_field('maptab'); ?>
               <li  rel="maptab<?php echo $count; ?>"><?php echo $maptab; ?></li>
               <?php 
                  $count++;
                  endwhile; ?>
            </ul>
            <?php endif; ?>      
            <div class="maptab_container">
               <?php if( have_rows('create_maps') ): 
                  $count = 1; 
                  while( have_rows('create_maps') ): the_row(); 
                  $maptab = get_sub_field('maptab');
                  if( have_rows('create_map') ): ?>
               <div id="maptab<?php echo $count; ?>" class="maptab_content">
                  <?php $count++; ?>
                  <?php while( have_rows('create_map') ): the_row(); 
                     $map_object = get_sub_field('map'); ?>
                        <?php echo $map_object; ?>
                  <?php endwhile; ?>
               </div>
               <?php endif; ?>
               <?php endwhile; ?>
               <?php endif; ?>
            </div>
         </div>
      </div>
      <!-- they trust us -->
      <div class="container trust">
         <?php if( get_field('trust') ): ?>
         <h2 class="titles"> <?php the_field('trust'); ?></h2>
         <div class="row">
         <?php endif; ?>
         <?php 
            if( have_rows('logo_carousel') ):?>
         <div class="logo-slider">
            <?php  while( have_rows('logo_carousel') ): the_row(); 
               $logo_img = get_sub_field('logo_img');
               $logo_link = get_sub_field('logo_link');
               ?>
            <?php if ($logo_link) {?>
            <a href="<?php echo $logo_link; ?>">
               <?php } ?>
               <div class="logo-item">
                  <img src="<?php echo $logo_img['url']; ?>" alt="<?php echo $logo_img['alt']; ?>">
               </div>
               <?php if($logo_link){ ?>
            </a>
            <?php } ?>
            <?php endwhile; ?>
         </div>
         <?php endif; ?>
         </div>
      </div>
      <!-- contact form -->
      <div class="container cf-7"id="contact" style="background-image: url('<?php the_field('cf_bg'); ?>')">
         <?php if( get_field('cf_title') ): ?>
         <h2 class="titles"> <?php the_field('cf_title'); ?></h2>
         <?php endif; ?>
         <div class="row">
            <div class="contact-row">
               <a href="tel:<?php the_field('contact_top_phone'); ?>"><i class="fa fa-phone" aria-hidden="true"></i> <span class="contacts-cf"><?php the_field('contact_top_phone'); ?></span></a> <span class="line-after"> | </span>
               <a href="mailto:<?php the_field('contact_top_email'); ?>"><i class="fa fa-envelope" aria-hidden="true"></i><span class="contacts-cf"><?php the_field('contact_top_email'); ?></span></a>
            </div>
            <?php echo do_shortcode('[contact-form-7 id="39" title="Contact form"]'); ?>
         </div>
      </div>
      <!-- Who we are -->
      <div class="container who-we" id="about">
         <div class="row">
            <?php if( get_field('who_we') ): ?>
            <h2 class="titles"> <?php the_field('who_we'); ?></h2>
            <?php endif; ?>
            <?php if( get_field('whowe_content') ): ?>
            <div class="whowe-content">
               <?php the_field('whowe_content'); ?>
            </div>
            <?php endif; ?>
         </div>
      </div>
   </main>
   <!-- #main -->
</div>
<!-- #primary -->
<?php echo
get_footer();