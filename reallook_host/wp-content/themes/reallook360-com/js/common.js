

jQuery(document).ready(function($){
    $('.testimonial-slider').slick({
    dots: true,
    slidesPerRow: 3,
    rows: 2,
    arrows: false,
    responsive: [
    {
      breakpoint: 991,
      settings: {
        slidesPerRow: 2,
        rows: 2,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 744,
      settings: {
        slidesPerRow: 1,
        infinite: true,
        dots: true
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});


//logo slider
$('.logo-slider').slick({
    dots: true,
    slidesPerRow: 4,
    rows: 2,
    arrows: false,
    responsive: [
    {
        breakpoint: 1199,
      settings: {
        slidesPerRow: 3,
        rows: 2,
        infinite: true,
        dots: true
      },
      breakpoint: 991,
      settings: {
        slidesPerRow: 3,
        rows: 2,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 640,
      settings: {
        slidesPerRow: 2,
        rows: 2,
        infinite: true,
        dots: true
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});

   jQuery(".head-panorama:before").click(function() {
       jQuery(this).css('display','none');
   });

    jQuery('#hamburger').click(function(){
        jQuery('body').toggleClass("menu-open");
    });




//zoom gallery
    jQuery('.zoom-gallery').magnificPopup({
            delegate: 'a',
            type: 'image',
            closeOnContentClick: false,
            closeBtnInside: false,
            mainClass: 'mfp-with-zoom mfp-img-mobile',
            image: {
                    verticalFit: true,
                    titleSrc: function(item) {
                            return item.el.attr('title') + ' &middot; <a class="image-source-link" href="'+item.el.attr('data-source')+'" target="_blank">image source</a>';
                    }
            },
            gallery: {
                    enabled: true
            },
            zoom: {
                    enabled: true,
                    duration: 300, // don't foget to change the duration also in CSS
                    opener: function(element) {
                            return element.find('img');
                    }
            }

    });
	jQuery(".menu").on("click","a", function (event) {
		//отменяем стандартную обработку нажатия по ссылке
		event.preventDefault();

		//забираем идентификатор бока с атрибута href
		var id  = jQuery(this).attr('href'),

		//узнаем высоту от начала страницы до блока на который ссылается якорь
			top = jQuery(id).offset().top;

		//анимируем переход на расстояние - top за 1500 мс
		jQuery('body,html').animate({scrollTop: top-180}, 1500);
	});


});
(function () {
	jQuery('.hamburger-menu').on('click', function() {
		jQuery('.bar').toggleClass('animate');
	});
})();

jQuery('.hamburger-menu').click(function(){
   jQuery(this).toggleClass('active-close');
   jQuery('.menu').toggleClass('active-menu');
});

//Tabs
 // tabbed content
    jQuery(".tab_content").hide();
    jQuery(".tab_content:first").show();

  /* if in tab mode */
    jQuery("ul.tabs li").click(function() {

      jQuery(".tab_content").hide();
      var activeTab = jQuery(this).attr("rel");
      jQuery("#"+activeTab).fadeIn(500);

      jQuery("ul.tabs li").removeClass("active");
      jQuery(this).addClass("active");

	  jQuery(".tab_drawer_heading").removeClass("d_active");
	  jQuery(".tab_drawer_heading[rel^='"+activeTab+"']").addClass("d_active");

    });
	/* if in drawer mode */
	jQuery(".tab_drawer_heading").click(function() {

      jQuery(".tab_content").hide();
      var d_activeTab = jQuery(this).attr("rel");
      jQuery("#"+d_activeTab).fadeIn(500);

	  jQuery(".tab_drawer_heading").removeClass("d_active");
      jQuery(this).addClass("d_active");

	  jQuery("ul.tabs li").removeClass("active");
	  jQuery("ul.tabs li[rel^='"+d_activeTab+"']").addClass("active");
    });


	/* Extra class "tab_last"
	   to add border to right side
	   of last tab */
	jQuery('ul.tabs li').last().addClass("tab_last");

        jQuery('ul.works-tab li:first-child').addClass('active');



        // map tabs
        //Tabs
 // tabbed content
    jQuery(".maptab_content").hide();
    jQuery(".maptab_content:first").show();

  /* if in tab mode */
    jQuery("ul.maptabs li").click(function() {

      jQuery(".maptab_content").hide();
      var activeTab = jQuery(this).attr("rel");
      jQuery("#"+activeTab).fadeIn(500);

      jQuery("ul.maptabs li").removeClass("active");
      jQuery(this).addClass("active");

	  jQuery(".maptab_drawer_heading").removeClass("d_active");
	  jQuery(".maptab_drawer_heading[rel^='"+activeTab+"']").addClass("d_active");

    });
	/* if in drawer mode */
	jQuery(".maptab_drawer_heading").click(function() {

      jQuery(".maptab_content").hide();
      var d_activeTab = jQuery(this).attr("rel");
      jQuery("#"+d_activeTab).fadeIn(500);

	  jQuery(".maptab_drawer_heading").removeClass("d_active");
      jQuery(this).addClass("d_active");

	  jQuery("ul.maptabs li").removeClass("active");
	  jQuery("ul.maptabs li[rel^='"+d_activeTab+"']").addClass("active");
    });


	/* Extra class "tab_last"
	   to add border to right side
	   of last tab */
	jQuery('ul.maptabs li').last().addClass("maptab_last");
	jQuery('.areas ul.works-tab li:first-child').addClass('active');

//parallax

jQuery(document).ready(function($){
    
  if ('.entry-modal iframe') {
    $(this).css('width','100%');
  }
  else {
      $('.entry-modal a.portfolio-btn').css('margin','0 auto');
  }
  $('.frames-wrapp p').css('display','none');
  var youtube_frame = $('.frames-wrapp iframe[src*="youtube"]');
  $(youtube_frame).unwrap();
  $(youtube_frame).parent().addClass('customize');

//add loop youtube frame
//  var youtube_modal = $('.entry-modal iframe[src*="youtube"]');
//$(youtube_modal).attr('loop','1');


});

//portfolio tabs
//
//    jQuery(".tab_content").hide();
//    jQuery(".tab_content:first").show();
//
//  /* if in tab mode */
//    jQuery("ul.tabs li").click(function() {
//
//      jQuery(".tab_content").hide();
//      var activeTab = jQuery(this).attr("rel");
//      jQuery("#"+activeTab).fadeIn(500);
//
//      jQuery("ul.tabs li").removeClass("active");
//      jQuery(this).addClass("active");
//
//	  jQuery(".tab_drawer_heading").removeClass("d_active");
//	  jQuery(".tab_drawer_heading[rel^='"+activeTab+"']").addClass("d_active");
//
//    });
//	/* if in drawer mode */
//	jQuery(".tab_drawer_heading").click(function() {
//
//      jQuery(".tab_content").hide();
//      var d_activeTab = jQuery(this).attr("rel");
//      jQuery("#"+d_activeTab).fadeIn(500);
//
//	  jQuery(".tab_drawer_heading").removeClass("d_active");
//      jQuery(this).addClass("d_active");
//
//	  jQuery("ul.tabs li").removeClass("active");
//	  jQuery("ul.tabs li[rel^='"+d_activeTab+"']").addClass("active");
//    });
//
//
//	/* Extra class "tab_last"
//	   to add border to right side
//	   of last tab */
//	jQuery('ul.tabs li').last().addClass("tab_last");
//
//        jQuery('ul.works-tab li:first-child').addClass('active');
//
//
