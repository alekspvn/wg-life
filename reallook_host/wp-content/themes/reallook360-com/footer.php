<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package reallook360.com
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer full-size" >
		<div class="site-info">
                    <?php if( get_field('footer_copyright') ): ?>      
                    <div class="powered"><?php the_field('footer_copyright'); ?></div>
                                <?php endif; ?>
                    <div class="contacts-line">         
                      <div class="contact-row">
                                    <a href="tel:<?php the_field('contact_top_phone'); ?>"><i class="fa fa-phone" aria-hidden="true"></i> <span class="contacts-cf"><?php the_field('contact_top_phone'); ?></span></a> <span class="line-after"> | </span>
                                    <a href="mailto:<?php the_field('contact_top_email'); ?>"><i class="fa fa-envelope" aria-hidden="true"></i><span class="contacts-cf"><?php the_field('contact_top_email'); ?></span></a>
                                 </div>
                        
                        <ul class="footer-social">
                            <?php if( have_rows('social_footer') ): ?>
                            <?php while( have_rows('social_footer') ): the_row();
                            $footer_link = get_sub_field('footer_link');
                            $footer_ico = get_sub_field('footer_ico'); ?>
                            <a href="<?php echo $footer_link ?>" target="blank"><li><?php echo $footer_ico; ?></li></a>
                        <?php endwhile; ?>
                        <?php endif; ?>
                        </ul>

                    <div class="footer-branding">
                            <?php
                            the_custom_logo(); ?>
                    </div><!-- .site-branding --><!-- .site-branding -->
                    </div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
