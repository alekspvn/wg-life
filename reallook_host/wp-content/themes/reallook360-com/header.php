<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package reallook360.com
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
        <script src="https://360player.io/static/dist/scripts/embed.js" async></script>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

	<header id="masthead" class="site-header">
            <div class="contact-line" id="home">
                <?php if( get_field('contact_top_phone') ): ?>
                                   <div class="top-contact-phone">
                                       <a class="link-top-contact" href="tel:<?php the_field('contact_top_phone'); ?>"><i class="fa fa-phone" aria-hidden="true"></i> <span><?php the_field('contact_top_phone'); ?></span></a>
                                   </div>
                               <?php endif; ?>
                <?php if( get_field('contact_top_email') ): ?>
                                   <div class="top-contact-email">
                                       <a class="link-top-contact" href="mailto:<?php the_field('contact_top_email'); ?>"><i class="fa fa-envelope" aria-hidden="true"></i> <span><?php the_field('contact_top_email'); ?></span></a>
                                   </div>
                               <?php endif; ?>
            </div>
            <div class="menu-container">
                    <div class="site-branding">
                            <?php
                            the_custom_logo(); ?>



                    </div><!-- .site-branding -->

                    <nav id="site-navigation" class="main-navigation">
                        <div class="hamburger-menu">
                             <div class="bar"></div>	
                        </div>
                            <?php
                                    wp_nav_menu( array(
                                            'theme_location' => 'menu-1',
                                            'menu_id'        => 'primary-menu',
                                    ) );
                            ?>
                    </nav><!-- #site-navigation -->
                    
                    
            </div>
	</header><!-- #masthead -->

	<div id="content" class="site-content">